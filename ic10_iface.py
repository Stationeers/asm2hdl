from asm_parse import get_cmd


class ic10_iface:
    def __init__(self, my_prg):
        self.io={}

        self.read_iface(my_prg)

    def read_iface(self, my_prg):
        for line in my_prg:
            print(line)
            cmd=get_cmd(line)
            offset=0
            if cmd == "l":
                self.update_io ("in", line[offset+2], line[offset+3])
            elif cmd == "s":
                self.update_io ("out", line[offset+1], line[offset+2])

    def update_io(self, dir, name, prop):
        if name not in self.io:
            self.io[name]={"in": [], "out": []}
        self.io[name][dir].append(prop)

    def print(self):
        for key, value in self.io.items():
            print(""+key+":")
            for key2, value2 in value.items():
                if len(value2) > 0:
                    print ("    "+key2+": "+str(value2))

    def generate_pkg(self):
        return ("package pkg is\n"+
            "\n"+
            self.write_types()+
            "end package;\n"
        )

    def write_types(self):
        ret=""
        for key, value in self.io.items():
            for key2, value2 in value.items():
                if len(value2) > 0:
                    ret+="    type t"+key+"_"+key2+" is record\n"
                    for i in value2:
                        ret+="        "+i+": real;\n"
                    ret+="    end record;\n\n"
        return ret

    def write_ports(self):
        lines=[]
        ret=""
        for key, value in self.io.items():
            for key2, value2 in value.items():
                if len(value2) > 0:
                    lines.append(key2[:1]+key+": "+key2+" t"+key+"_"+key2)
        for i in range(0, len(lines)):
            ret+=lines[i]
            if i < len(lines)-1:
                ret+=";"
            ret+="\n"
        return ret
