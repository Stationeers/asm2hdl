from asm_parse import get_cmd
from asm_parse import get_arg
from asm_parse import is_reg


def generate_regs(n):
    ret=""
    for i in range(0, n):
        ret+="variable r"+str(i)+": real;\n"
    return ret

def parse_regs(prg):
    ret=[]
    for i in prg:
        new_regs=parse_regs_from_line(i)
        ret=merge_regs(ret, new_regs)
    return ret

def parse_regs_from_line(line):
    ret=[]
    cmd=get_cmd(line)
    if cmd == "l":
        append_if_reg(ret, get_arg(line, 0))
    elif cmd == "xor":
        append_if_reg(ret, get_arg(line, 0))
        append_if_reg(ret, get_arg(line, 1))
        append_if_reg(ret, get_arg(line, 2))
    elif cmd == "yield":
        pass
    elif cmd == "j":
        append_if_reg(ret, get_arg(line, 0))
    elif cmd == "s":
        append_if_reg(ret, get_arg(line, 2))
    else:
        raise Exception ("unknown command: "+cmd)
    return ret

def append_if_reg(ret, new):
    if is_reg(new):
        ret.append(new)

def merge_regs(old, new):
    in_old=set(old)
    in_new=set(new)
    to_add=in_new-in_old
    return old+list(to_add)

class ic10_regs:
    def __init__(self, prg):
        self.regs=parse_regs(prg)

    def generate_variables(self):
        ret=""
        print ("regs"+str(self.regs))
        for i in self.regs:
            ret+="variable "+i+": real;\n"
        return ret

    def parse_regs(prg):
        ret=[]

    def replace_if_constant(self, name):
        if is_reg(name):
            return name
        else:
            return "real(name)"
