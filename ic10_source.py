from ic10_iface import ic10_iface
from ic10_regs import ic10_regs
from asm_parse import get_cmd
from asm_parse import get_arg
from asm_parse import is_reg


def indent (n, str):
    spaces=""
    for i in range(0, n):
        spaces+="    "
    ret=""
    lines=str.splitlines()
    for l in lines:
        ret+=spaces+l+"\n"
    return ret

def print_lut_keys(lut):
    ret=""
    keys=list(lut.keys())
    for i in range(0, len(keys)):
        ret+="        "+keys[i]
        if i < len(lut)-1:
            ret += ","
        ret += "\n"
    return ret

def print_lut_map(lut):
    ret=""
    keys=list(lut.keys())
    for i in range(0, len(keys)):
        ret+="        "+str(keys[i])+" => "+str(lut[keys[i]])
        if i < len(lut)-1:
            ret += ","
        ret += "\n"
    return ret

class ic10_source:
    def __init__(self, prg, io_cfg):
        self.prg=prg
        self.io_cfg=io_cfg
        self.reg_cfg=ic10_regs(prg)

    def generate_source(self):
        return self.write_entity()+self.write_architecture()

    def write_entity(self):
        return (
            "library work;\n"
            "use work.pkg.all;\n"
            "\n"
            "entity ic10 is\n"
            "    port (\n"+
            "        iClk: in std_logic;\n"+
            indent(2, self.io_cfg.write_ports())+
            "    );\n"
            "end entity;\n"
            "\n"
        )

    def write_architecture(self):
        return ("architecture v1 of ic10 is\n"
            "\n"+
            self.write_lable_lut()+
            "\n"
            "begin\n"
            "\n"
            "    process (iClk)\n"
            "\n"+indent(2, self.reg_cfg.generate_variables())+
            "\n"
            "        procedure execute (\n"
            "            line_number: natural;\n"
            "            variable next_line_number: out natural;\n"
            "            variable yield: out boolean\n"
            "        ) return boolean is\n"
            "        begin\n"
            "            next_line_number := line_number +1;\n"
            "            yield := false;\n"
            "\n"
            "            case line_number is\n"+
            indent(4, self.generate_execute_procedure())+
            "            end case;\n"
            "        end procedure;\n"
            "\n"
            "        variable line_cnt: natural := 0;\n"
            "        variable yield: boolean := false;\n"
            "\n"
            "    begin\n"
            "\n"
            "        while not yield loop\n"
            "            execute (line_cnt, line_cnt, yield);\n"
            "        end loop;\n"
            "\n"
            "    end process;\n"
            "\n"
            "end v1;\n"
        )

    def write_lable_lut(self):
        lut=self.get_lable_lut()
        ret="    type tLable_lut_keys is (\n"+print_lut_keys(lut)
        ret+=("    );\n"
            "\n"
            "    type tLable_lut is array (tLable_lut_keys) of natural;\n"
            "\n"
            "    constant cLable_lut: tLable_lut := (\n"+
            print_lut_map(lut)+
            "    );\n"
        )
        return ret

    def get_lable_lut(self):
        lut={}
        for i in range(0, len(self.prg)):
            if self.prg[i][0][-1:] == ":":
                lut[self.prg[i][0][:-1]]=i
        return lut

    def generate_execute_procedure(self):
        ret=""
        for i in range (0, len(self.prg)):
            ret+="when "+str(i)+":\n"+indent(1, self.generate_rule_for_line(i))+"\n"
        return ret

    def generate_rule_for_line(self, i):
        line=self.prg[i]
        cmd=get_cmd(line)
        print ("when for cmd "+cmd)
        ret="print (\"executing line: '"+' '.join(self.prg[i])+"'\";\n"
        if cmd == "j":
            ret+="next_line_number := cLable_lut("+get_arg(line, 0)+");\n"
        elif cmd == "l":
            return get_arg(line, 0)+" := i"+get_arg(line, 1)+"."+get_arg(line, 2)+";\n"
        elif cmd == "xor":
            arg0=self.reg_cfg.replace_if_constant(get_arg(line, 1))
            arg1=self.reg_cfg.replace_if_constant(get_arg(line, 2))
            ret+=("if ("+arg0+">0) xor ("+arg1+">0) then\n"
                "    "+get_arg(line, 0)+" := 1;\n"
                "else\n"
                "    "+get_arg(line, 0)+" := 0;\n"
                "end if;\n"
            )
        elif cmd == "s":
            ret+="o"+get_arg(line, 0)+"."+get_arg(line, 1)+" := "+get_arg(line, 2)+";\n"
        elif cmd == "yield":
            ret+="yield := true;\n"
        else:
            raise Exception ("unknown command: "+cmd)
        return ret
