import sys
from hdl_project import hdl_project


def my_split(str):
    lines=str.splitlines()
    ret=[]
    for l in lines:
        ret.append(l.split())
    return ret

with open(sys.argv[1]) as file:
    prg=my_split(file.read())

prj=hdl_project(prg)
