
def get_cmd(words):
    if is_lable(words[0]):
        return words[1]
    else:
        return words[0]

def get_arg(words, n):
    if is_lable(words[0]):
        n+=1
    return words[n+1]

def is_lable(word):
    return word[-1:] == ":"

def is_reg(name):
    for i in range (0, 18):
        return name == "r"+str(i)
