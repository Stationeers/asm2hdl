import pathlib
from ic10_iface import ic10_iface
from ic10_source import ic10_source


PATH_SEPARATOR='/'
HDL_PATH='hdl'+PATH_SEPARATOR
SRC_PATH=HDL_PATH+'src'+PATH_SEPARATOR
PKG_PATH=SRC_PATH+'pkg'+PATH_SEPARATOR

class hdl_project:
    def __init__(self, prg):
        self.io_cfg=ic10_iface(prg)
        self.io_cfg.print()
        self.source=ic10_source(prg, self.io_cfg)
        self.dump()

    def dump(self):
        pathlib.Path(PKG_PATH).mkdir(parents=True, exist_ok=True)
        with open(PKG_PATH+"pkg.vhd", "w") as file:
            file.write(self.io_cfg.generate_pkg())

        with open(SRC_PATH+"ic10.vhd", "w") as file:
            file.write(self.source.generate_source())

